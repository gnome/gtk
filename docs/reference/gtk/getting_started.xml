<?xml version="1.0"?>
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.3//EN"
               "http://www.oasis-open.org/docbook/xml/4.3/docbookx.dtd" [
]>
<chapter id="gtk-getting-started" xmlns:xi="http://www.w3.org/2003/XInclude">
  <title>Getting Started with GTK+</title>

  <para>This chapter is contains some tutorial information to get you
  started with GTK+ programming. It assumes that you have GTK+, its
  dependencies and a C compiler installed and ready to use. If you
  need to build GTK+ itself first, refer to the
  <link linkend="gtk-compiling">Compiling the GTK+ libraries</link>
  section in this reference.</para>

  <para>To begin our introduction to GTK, we'll start with the simplest
  program possible. This program will create an empty 200x200 pixel
  window:</para>

  <para>
    <inlinegraphic fileref="window-default.png" format="PNG"></inlinegraphic>
  </para>

  <informalexample><programlisting>
    <xi:include href="../../../../examples/window-default.c" parse="text">
      <xi:fallback>FIXME: MISSING XINCLUDE CONTENT</xi:fallback>
    </xi:include>
  </programlisting></informalexample>

  <para>You can compile the program above with GCC using:</para>

  <para><literallayout>
    <literal>gcc `pkg-config --cflags gtk+-3.0` -o window-default window-default.c `pkg-config --libs gtk+-3.0`</literal>
  </literallayout></para>

  <note><para>For more information on how to compile a GTK+ application, please
  refer to the <link linkend="gtk-compiling">Compiling GTK+ Applications</link>
  section in this reference.</para></note>

  <para>All GTK+ applications will, of course, include
  <filename>gtk/gtk.h</filename>, which declares functions, types and
  macros required by GTK+ applications.</para>

  <warning><para>Even if GTK+ installs multiple header files, only the
  top-level <filename>gtk/gtk.h</filename> header can be directly included
  by third party code. The compiler will abort with an error if any other
  header will be included.</para></warning>

  <para>We then proceed into the <function>main</function>() function of the
  application, and we declare a <varname>window</varname> variable as a pointer
  of type #GtkWidget.</para>

  <para>The following line will call gtk_init(), which
  is the initialization function for GTK+; this function will set up GTK+,
  the type system, the connection to the windowing environment, etc. The
  gtk_init() takes as arguments the pointers to the command line arguments
  counter and string array; this allows GTK+ to parse specific command line
  arguments that control the behavior of GTK+ itself. The parsed arguments
  will be removed from the array, leaving the unrecognized ones for your
  application to parse.</para>

  <note><para>For more information on which command line arguments GTK+
  recognizes, please refer to the <link linkend="gtk-running">Running GTK+
  Applications</link> section in this reference.</para></note>

  <para>The call to gtk_window_new() will create a new #GtkWindow and store
  it inside the <varname>window</varname> variable. The type of the window
  is %GTK_WINDOW_TOPLEVEL, which means that the #GtkWindow will be managed
  by the windowing system: it will have a frame, a title bar and window
  controls, depending on the platform.</para>

  <para>In order to terminate the application when the #GtkWindow is
  destroyed, we connect the #GtkWidget::destroy signal to the gtk_main_quit()
  function. This function will terminate the GTK+ main loop started by calling
  gtk_main() later. The #GtkWidget::destroy signal is emitted when a widget is
  destroyed, either by explicitly calling gtk_widget_destroy() or when the
  widget is unparented. Top-level #GtkWindow<!-- -->s are also destroyed when
  the Close window control button is clicked.</para>

  <para>#GtkWidget<!-- -->s are hidden by default. By calling gtk_widget_show()
  on a #GtkWidget we are asking GTK+ to set the visibility attribute so that it
  can be displayed. All this work is done after the main loop has been
  started.</para>

  <para>The last line of interest is the call to gtk_main(). This function will
  start the GTK+ main loop and will block the control flow of the
  <function>main</function>() until the gtk_main_quit() function is
  called.</para>

  <para>The following example is slightly more complex, and tries to
  showcase some of the capabilities of GTK+.</para>

  <para>In the long tradition of programming languages and libraries,
  it is called <emphasis>Hello, World</emphasis>.</para>

  <para>
    <inlinegraphic fileref="hello-world.png" format="PNG"></inlinegraphic>
  </para>

  <example id="gtk-getting-started-hello-world">
    <title>Hello World in GTK+</title>
    <programlisting>
      <xi:include href="../../../../examples/hello-world.c" parse="text">
        <xi:fallback>FIXME: MISSING XINCLUDE CONTENT</xi:fallback>
      </xi:include>
    </programlisting>
  </example>

</chapter>
